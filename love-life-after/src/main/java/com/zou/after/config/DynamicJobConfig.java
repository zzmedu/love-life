package com.zou.after.config;

import cn.hutool.extra.mail.MailAccount;
import com.zou.after.task.base.TaskCommon;
import com.zou.after.entity.DynamicTask;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.util.StringUtils;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;

/**
 * 动态化定时任务
 */

@Configuration
@EnableScheduling
@Slf4j
public class DynamicJobConfig implements SchedulingConfigurer {

    private volatile ScheduledTaskRegistrar scheduledTaskRegistrar;
    /**
     * 任务调度器列表
     */
    private final ConcurrentHashMap<Long, ScheduledFuture<?>> scheduledFutureConcurrentHashMap = new ConcurrentHashMap<>();
    /**
     * 定时任务列表
     */
    private final ConcurrentHashMap<Long, CronTask> cronTaskConcurrentHashMap = new ConcurrentHashMap<>();

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {

        //单机串行；默认单线程
        scheduledTaskRegistrar.setScheduler(Executors.newScheduledThreadPool(30));

        this.scheduledTaskRegistrar = scheduledTaskRegistrar;
    }

    /**
     * 刷新任务
     */
    public void refresh(List<DynamicTask> dynamicTasks) {
        /**
         * 防止定时任务删除，而定时任务还在执行
         */
        Set<Long> ids = scheduledFutureConcurrentHashMap.keySet();
        for (Long id : ids) {
            if (!exists(dynamicTasks, id)) {
                //在定时任务列表中去除当前id标示的那个ScheduledFuture
                scheduledFutureConcurrentHashMap.get(id).cancel(false);
            }
        }
        /**
         * 定时任务更新
         */
        if (!dynamicTasks.isEmpty()) {
            for (DynamicTask dynamicTask : dynamicTasks) {
                // 判断计划已存在并且表达式未发生则跳过执行
                if (scheduledFutureConcurrentHashMap.containsKey(dynamicTask.getId()) && cronTaskConcurrentHashMap.get(dynamicTask.getId()).getExpression().equals(dynamicTask.getCron())) {
                    continue;
                }
                // 如果定时任务表达式发生了变化，则取消当前调度任务；刷新其实就是，在内存中先删除原始数据，在导入新数据
                if (scheduledFutureConcurrentHashMap.containsKey(dynamicTask.getId())) {
                    // 取消调度器
                    scheduledFutureConcurrentHashMap.get(dynamicTask.getId()).cancel(false);
                    // 删除调度器
                    scheduledFutureConcurrentHashMap.remove(dynamicTask.getId());
                    // 删除调度任务
                    cronTaskConcurrentHashMap.remove(dynamicTask.getId());
                }

                CronTask cronTask = new CronTask(new Runnable() {
                    @SneakyThrows
                    @Override
                    public void run() {
                        if(!StringUtils.isEmpty(dynamicTask.getClassPath())){
                            //执行当前的任务逻辑
                            TaskCommon taskCommon = (TaskCommon)Class.forName(dynamicTask.getClassPath()).newInstance();
                            taskCommon.execJob();
                        }
                    }
                }, dynamicTask.getCron());
                //加入定时任务
                cronTaskConcurrentHashMap.put(dynamicTask.getId(), cronTask);
                //加入调度器
                scheduledFutureConcurrentHashMap.put(dynamicTask.getId(), scheduledTaskRegistrar.getScheduler().schedule(cronTask.getRunnable(), cronTask.getTrigger()));
            }
        }

    }

    /**
     * 判断是否有该ID任务
     */
    private boolean exists(List<DynamicTask> dynamicTasks, Long id) {
        if (!dynamicTasks.isEmpty()) {
            for (DynamicTask dynamicTask : dynamicTasks) {
                if (dynamicTask.getId().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 用来修饰一个非静态的void()方法.而且这个方法不能有抛出异常声明
     */
    @PreDestroy
    public void destroy() {
        this.scheduledTaskRegistrar.destroy();
    }
}
