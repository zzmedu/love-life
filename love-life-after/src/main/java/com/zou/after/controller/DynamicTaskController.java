package com.zou.after.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zou.after.config.DynamicJobConfig;
import com.zou.after.dto.DynamicTaskAddDto;
import com.zou.after.dto.DynamicTaskFindByPageDto;
import com.zou.after.dto.DynamicTaskUpdateDto;
import com.zou.after.entity.DynamicTask;
import com.zou.after.service.IDynamicTaskService;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zzm.zlog.inter.ZLog;

import javax.validation.Valid;

@RestController
@RequestMapping("dynamicTask")
public class DynamicTaskController {

    @Autowired
    private IDynamicTaskService iDynamicTaskService;
    @Autowired
    private DynamicJobConfig dynamicJobConfig;

    @PostMapping("/findByPage")
    @ZLog(value = "分页查询")
    public Response findByPage(@Valid @RequestBody DynamicTaskFindByPageDto dynamicTaskFindByPageDto) {
        return Request.success(iDynamicTaskService.findByPage(dynamicTaskFindByPageDto), iDynamicTaskService.findByPageCount(dynamicTaskFindByPageDto));
    }

    @PostMapping("/add")
    @ZLog(value = "新增")
    public Response add(@Valid @RequestBody DynamicTaskAddDto dynamicTaskAddDto) {
        DynamicTask dynamicTask = new DynamicTask();
        BeanUtils.copyProperties(dynamicTaskAddDto, dynamicTask);
        if (iDynamicTaskService.save(dynamicTask)) {
            dynamicJobConfig.refresh(iDynamicTaskService.list(new QueryWrapper<DynamicTask>().lambda().eq(DynamicTask::getStatus,1)));
            return Request.success();
        }
        return Request.error();
    }

    @GetMapping("/delete/{id}")
    @ZLog(value = "非逻辑删除数据")
    public Response delete(@PathVariable Integer id) {
        if (iDynamicTaskService.remove(new QueryWrapper<DynamicTask>().lambda().eq(DynamicTask::getId, id))) {
            dynamicJobConfig.refresh(iDynamicTaskService.list(new QueryWrapper<DynamicTask>().lambda().eq(DynamicTask::getStatus,1)));
            return Request.success();
        }
        return Request.error();
    }

    @PostMapping("/update")
    @ZLog(value = "修改")
    public Response update(@Valid @RequestBody DynamicTaskUpdateDto dynamicTaskUpdateDto) {
        DynamicTask dynamicTask = new DynamicTask();
        BeanUtils.copyProperties(dynamicTaskUpdateDto, dynamicTask);
        dynamicTask.setStatus(1);
        if (iDynamicTaskService.update(dynamicTask, new QueryWrapper<DynamicTask>().lambda().eq(DynamicTask::getId, dynamicTaskUpdateDto.getId()))) {
            dynamicJobConfig.refresh(iDynamicTaskService.list(new QueryWrapper<DynamicTask>().lambda().eq(DynamicTask::getStatus,1)));
            return Request.success();
        }
        return Request.error();
    }

    @PostMapping("/updateStatus/{id}/{status}")
    @ZLog(value = "修改状态")
    public Response updateStatus(@PathVariable String id,@PathVariable String status) {
        if (iDynamicTaskService.update(new UpdateWrapper<DynamicTask>().lambda().set(DynamicTask::getStatus,status).eq(DynamicTask::getId, id))) {
            dynamicJobConfig.refresh(iDynamicTaskService.list(new QueryWrapper<DynamicTask>().lambda().eq(DynamicTask::getStatus,1)));
            return Request.success();
        }
        return Request.error();
    }

}
