package com.zou.after.dto;

import com.zou.after.dto.base.LayuiDto;
import lombok.Data;

@Data
public class DynamicTaskFindByPageDto extends LayuiDto {
    private String name;
    private String bear;
    private Integer status;
}
