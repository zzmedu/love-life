package com.zou.after.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
public class MoneyIncomeAddDto {

	/** 收入方式编号 */
	@NotBlank(message="收入方式编号不能为空")
    private Long modeId;

	/** 收入方式类型 */
	@NotBlank(message="收入方式类型不能为空")
    private Long typeId;

	/** 收入金额 */
	@NotBlank(message="收入金额不能为空")
    private BigDecimal money;

	/** 支付时间 */
	@NotBlank(message="支付时间不能为空")
    private String stime;

	/** 修改时间 */
	@NotBlank(message="修改时间不能为空")
    private String utime;

	/** 扣费商家 */
	@NotBlank(message="扣费商家不能为空")
    private String store;

	/** 收入用户编号 */
	@NotBlank(message="收入用户编号不能为空")
    private Long userId;
}