package com.zou.after.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class MoneyModeAddDto {

	/** 支付方式名称 */
	@NotBlank(message="支付方式名称不能为空")
    private String name;
}