package com.zou.after.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
public class MoneyPayAddDto {

	/** 编号 */

	/** 状态：1：正常；2：删除 */
	@NotBlank(message="状态：1：正常；2：删除不能为空")
    private Integer status;

	/** 支付方式编号 */
	@NotBlank(message="支付方式编号不能为空")
    private Long modeId;

	/** 所属类型编号；生活，采购，衣/食/住/行 */
	@NotBlank(message="所属类型编号；生活，采购，衣/食/住/行不能为空")
    private Long typeId;

	/** 支付金额 */
	@NotBlank(message="支付金额不能为空")
    private BigDecimal money;

	/** 支付时间 */
	@NotBlank(message="支付时间不能为空")
    private String stime;

	/** 修改时间 */
	@NotBlank(message="修改时间不能为空")
    private String utime;

	/** 扣费商家 */
	@NotBlank(message="扣费商家不能为空")
    private String store;

	/** 支付用户编号 */
	@NotBlank(message="支付用户编号不能为空")
    private Long userId;
}