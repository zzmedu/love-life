package com.zou.after.dto.base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

@Data
@EqualsAndHashCode(callSuper = false)
public class BaseDto extends LayuiDto {

    private String startDate;

    private String endDate;

    public void setStartDate(String startDate){
        if(!StringUtils.isEmpty(startDate)){
            String[] time = startDate.replaceAll(" ","").split("~");
            this.startDate = time[0];
            this.endDate = time[1];
        }
    }

    private String field;

    public String getField() {
        //驼峰转下划线
        return this.field;
    }

    private String type;

    public String getType() {
        if(StringUtils.isEmpty(this.type)){
            this.type = "desc";
        }
        return this.type;
    }
}
