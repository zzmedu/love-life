package com.zou.after.dto.base;

import cn.hutool.core.date.DateUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 16:01 2020/6/28
 * </P>
 **/

@Data
@EqualsAndHashCode(callSuper = false)
public class MonthDto extends LayuiDto {

    private String startDate;

    private String endDate;

    public void setStartDate(String startDate){
        if(!StringUtils.isEmpty(startDate)){
            String[] time = startDate.replaceAll(" ","").split("~");
            this.startDate = time[0];
            this.endDate = time[1];
        }
    }

    public String getStartDate(){
        if(StringUtils.isEmpty(startDate)){
            this.startDate = DateUtil.formatDate(DateUtil.lastMonth());
            this.endDate = DateUtil.today();
        }
        return this.startDate;
    }

    private String field;

    public String getField() {
        //驼峰转下划线
        return this.field;
    }

    private String type;

    public String getType() {
        if(StringUtils.isEmpty(this.type)){
            this.type = "desc";
        }
        return this.type;
    }
}
