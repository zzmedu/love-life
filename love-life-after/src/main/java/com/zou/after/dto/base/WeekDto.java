package com.zou.after.dto.base;

import cn.hutool.core.date.DateUtil;
import lombok.Data;
import org.springframework.util.StringUtils;

@Data
public class WeekDto {

    private String startDate;

    private String endDate;

    public void setStartDate(String startDate){
        if(!StringUtils.isEmpty(startDate)){
            String[] time = startDate.replaceAll(" ","").split("~");
            this.startDate = time[0];
            this.endDate = time[1];
        }
    }

    public String getStartDate(){
        if(StringUtils.isEmpty(startDate)){
            this.startDate = DateUtil.formatDate(DateUtil.lastWeek());
            this.endDate = DateUtil.today();
        }
        return this.startDate;
    }
}
