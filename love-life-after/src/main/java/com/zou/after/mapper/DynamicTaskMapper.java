package com.zou.after.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zou.after.dto.DynamicTaskFindByPageDto;
import com.zou.after.entity.DynamicTask;
import com.zou.after.mapper.base.FindByPageMapper;
import com.zou.after.vo.DynamicTaskFindByPageVo;

public interface DynamicTaskMapper extends BaseMapper<DynamicTask>, FindByPageMapper<DynamicTaskFindByPageDto, DynamicTaskFindByPageVo> {
}
