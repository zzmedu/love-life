package com.zou.after.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zou.after.mapper.base.FindByPageMapper;
import com.zou.after.dto.MoneyIncomeFindByPageDto;
import com.zou.after.vo.MoneyIncomeFindByPageVo;
import com.zou.after.entity.MoneyIncome;

public interface MoneyIncomeMapper extends BaseMapper<MoneyIncome>,FindByPageMapper<MoneyIncomeFindByPageDto,MoneyIncomeFindByPageVo>{

}