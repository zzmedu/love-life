package com.zou.after.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zou.after.mapper.base.FindByPageMapper;
import com.zou.after.dto.MoneyModeFindByPageDto;
import com.zou.after.vo.MoneyModeFindByPageVo;
import com.zou.after.entity.MoneyMode;

public interface MoneyModeMapper extends BaseMapper<MoneyMode>,FindByPageMapper<MoneyModeFindByPageDto,MoneyModeFindByPageVo>{

}