package com.zou.after.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zou.after.mapper.base.FindByPageMapper;
import com.zou.after.dto.MoneyPayFindByPageDto;
import com.zou.after.vo.MoneyPayFindByPageVo;
import com.zou.after.entity.MoneyPay;

public interface MoneyPayMapper extends BaseMapper<MoneyPay>,FindByPageMapper<MoneyPayFindByPageDto,MoneyPayFindByPageVo>{

}