package com.zou.after.mapper.base;

import java.util.List;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：分页接口封装
 * @date：crealed in 10:53 2019/8/26
 * </P>
 **/
public interface FindByPageMapper<T,V> {

    /**
     * 分页查询
     * @param t 查询条件
     * @return List<V>
     */
    List<V> findByPage(T t);

    /**
     * 分页查询总数
     * @param t 查询条件
     * @return int
     */
    Integer findByPageCount(T t);
}
