package com.zou.after.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.after.dto.DynamicTaskFindByPageDto;
import com.zou.after.entity.DynamicTask;
import com.zou.after.service.base.IFindByPageService;
import com.zou.after.vo.DynamicTaskFindByPageVo;

public interface IDynamicTaskService extends IService<DynamicTask>, IFindByPageService<DynamicTaskFindByPageDto, DynamicTaskFindByPageVo> {
}
