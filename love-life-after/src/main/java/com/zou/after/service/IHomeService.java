package com.zou.after.service;

import com.zou.after.vo.HomeIncomeToDayVo;
import com.zou.security.base.http.Response;

public interface IHomeService {
    HomeIncomeToDayVo incomeModeToDay(String time);
    HomeIncomeToDayVo incomeTypeToDay(String time);
    HomeIncomeToDayVo payModeToDay(String time);
    HomeIncomeToDayVo payTypeToDay(String time);
    Response aweekDiscount(String stime, String etime);
}
