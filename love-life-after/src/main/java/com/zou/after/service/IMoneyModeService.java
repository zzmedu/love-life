package com.zou.after.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.after.dto.MoneyModeFindByPageDto;
import com.zou.after.service.base.IFindByPageService;
import com.zou.after.vo.MoneyModeFindByPageVo;
import com.zou.after.entity.MoneyMode;
/**
* com.zou.after服务类
* @author zouzhimin
* @date $date.get()
*/
public interface IMoneyModeService extends IService<MoneyMode>, IFindByPageService<MoneyModeFindByPageDto, MoneyModeFindByPageVo>{
	
}