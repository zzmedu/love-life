package com.zou.after.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.after.dto.MoneyPayFindByPageDto;
import com.zou.after.service.base.IFindByPageService;
import com.zou.after.vo.MoneyPayFindByPageVo;
import com.zou.after.entity.MoneyPay;
/**
* com.zou.after服务类
* @author zouzhimin
* @date $date.get()
*/
public interface IMoneyPayService extends IService<MoneyPay>, IFindByPageService<MoneyPayFindByPageDto, MoneyPayFindByPageVo>{
	
}