package com.zou.after.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zou.after.dto.MoneyIncomeFindByPageDto;
import com.zou.after.service.IMoneyIncomeService;
import com.zou.after.vo.MoneyIncomeFindByPageVo;
import com.zou.after.entity.MoneyIncome;
import com.zou.after.mapper.MoneyIncomeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoneyIncomeServiceImpl extends ServiceImpl<MoneyIncomeMapper, MoneyIncome> implements IMoneyIncomeService{

    @Autowired
    private MoneyIncomeMapper moneyIncomeMapper;
    
    /**
     * 分页查询
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public List<MoneyIncomeFindByPageVo> findByPage(MoneyIncomeFindByPageDto moneyIncomeFindByPageDto) {
        return moneyIncomeMapper.findByPage(moneyIncomeFindByPageDto);
    }

	/**
     * 分页查询总数
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public Integer findByPageCount(MoneyIncomeFindByPageDto moneyIncomeFindByPageDto) {
        return moneyIncomeMapper.findByPageCount(moneyIncomeFindByPageDto);
    }
	
}