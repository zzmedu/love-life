package com.zou.after.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zou.after.dto.MoneyModeFindByPageDto;
import com.zou.after.service.IMoneyModeService;
import com.zou.after.vo.MoneyModeFindByPageVo;
import com.zou.after.entity.MoneyMode;
import com.zou.after.mapper.MoneyModeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoneyModeServiceImpl extends ServiceImpl<MoneyModeMapper, MoneyMode> implements IMoneyModeService{

    @Autowired
    private MoneyModeMapper moneyModeMapper;
    
    /**
     * 分页查询
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public List<MoneyModeFindByPageVo> findByPage(MoneyModeFindByPageDto moneyModeFindByPageDto) {
        return moneyModeMapper.findByPage(moneyModeFindByPageDto);
    }

	/**
     * 分页查询总数
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public Integer findByPageCount(MoneyModeFindByPageDto moneyModeFindByPageDto) {
        return moneyModeMapper.findByPageCount(moneyModeFindByPageDto);
    }
	
}