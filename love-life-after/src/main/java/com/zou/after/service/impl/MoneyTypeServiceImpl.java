package com.zou.after.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zou.after.dto.MoneyTypeFindByPageDto;
import com.zou.after.service.IMoneyTypeService;
import com.zou.after.vo.MoneyTypeFindByPageVo;
import com.zou.after.entity.MoneyType;
import com.zou.after.mapper.MoneyTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoneyTypeServiceImpl extends ServiceImpl<MoneyTypeMapper, MoneyType> implements IMoneyTypeService{

    @Autowired
    private MoneyTypeMapper moneyTypeMapper;
    
    /**
     * 分页查询
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public List<MoneyTypeFindByPageVo> findByPage(MoneyTypeFindByPageDto moneyTypeFindByPageDto) {
        return moneyTypeMapper.findByPage(moneyTypeFindByPageDto);
    }

	/**
     * 分页查询总数
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public Integer findByPageCount(MoneyTypeFindByPageDto moneyTypeFindByPageDto) {
        return moneyTypeMapper.findByPageCount(moneyTypeFindByPageDto);
    }
	
}