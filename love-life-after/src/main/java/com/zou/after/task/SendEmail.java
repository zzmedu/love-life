package com.zou.after.task;

import cn.hutool.core.date.DateUtil;
import cn.hutool.extra.mail.MailUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zou.after.mapper.HomeMapper;
import com.zou.after.task.base.TaskCommon;
import com.zou.after.vo.HomeAweekDiscountVo;
import com.zou.security.base.pojo.SysUser;
import com.zou.security.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SendEmail extends TaskCommon {

    private static final String TABLE = "style=\"font-family:Trebuchet MS\",Arial,Helvetica,sans-serif;width:100%;border-collapse:collapse\"";
    private static final String TR = "style=\"background-color: #428BCA; color:#ffffff\">";
    private static final String TH = "style=\"border-width:1px;border-style:solid;border-color:#e6e6e6;position:relative;padding:9px 15px;min-height:30px;line-height:30px;font-size:14px;padding-top: 5px;padding-bottom: 4px;background-color: #f2f2f2;color: #000\"";
    private static final String TD = "style=\"border-width:1px;border-style:solid;border-color:#e6e6e6;position:relative;padding:9px 15px;min-height:30px;line-height:30px;font-size:14px;\"";

    public static SendEmail sendEmail;

    @Autowired
    private HomeMapper homeMapper;
    @Autowired
    private SysUserMapper sysUserMapper;

    @PostConstruct
    public void init() {
        sendEmail = this;
        sendEmail.homeMapper = this.homeMapper;
        sendEmail.sysUserMapper = this.sysUserMapper;
    }

    /**
     * 日报
     */
    @Override
    public void execJob() {
        StringBuilder content = new StringBuilder("<html><head></head><body><h2>近七天收入和支出数据</h2>");
        content.append("<div style=\"width: 98%;margin: 5px auto\">");
        content.append("<table "+TABLE+"");
        content.append("<tr "+TR+"");
        content.append("<th "+TH+">日期</th>");
        content.append("<th "+TH+">收入</th>");
        content.append("<th "+TH+">支出</th>");
        content.append("<th "+TH+">利润</th>");
        content.append("</tr>");
        List<HomeAweekDiscountVo> homeAweekDiscountVos = sendEmail.homeMapper.aweekDiscount(DateUtil.formatDate(DateUtil.offsetDay(DateUtil.lastWeek(), +1)), DateUtil.today());
        for (HomeAweekDiscountVo homeAweekDiscountVo : homeAweekDiscountVos) {
            content.append("<tr>");
            content.append("<td "+TD+">" + homeAweekDiscountVo.getDate() + "</td>");
            content.append("<td "+TD+">" + homeAweekDiscountVo.getIncomeMoney() + "</td>");
            content.append("<td "+TD+">" + homeAweekDiscountVo.getPayMoney() + "</td>");
            content.append("<td "+TD+">" + new BigDecimal(homeAweekDiscountVo.getIncomeMoney()).subtract(new BigDecimal(homeAweekDiscountVo.getPayMoney())) + "</td>");
            content.append("</tr>");
        }
        /**
        * 合计
        * */
        content.append("<tr>");
        content.append("<td "+TD+">合计</td>");
        content.append("<td "+TD+">"+homeAweekDiscountVos.stream().map(o->o.getIncomeMoney()).map(BigDecimal::new).reduce(BigDecimal.ZERO,BigDecimal::add).toString()+"</td>");
        content.append("<td "+TD+">"+homeAweekDiscountVos.stream().map(o->o.getPayMoney()).map(BigDecimal::new).reduce(BigDecimal.ZERO,BigDecimal::add).toString()+"</td>");
        content.append("<td "+TD+">"+homeAweekDiscountVos.stream().map(o-> new BigDecimal(o.getIncomeMoney()).subtract(new BigDecimal(o.getPayMoney()))).reduce(BigDecimal.ZERO,BigDecimal::add).toString()+"</td>");
        content.append("</tr>");
        content.append("</table>");
        content.append("</div>");
        content.append("</body></html>");
        List<String> email = sendEmail.sysUserMapper.selectList(new QueryWrapper<SysUser>().lambda()
                .select(SysUser::getEmail)
                .eq(SysUser::getEnabled, 1).apply("email != ''"))
                .stream()
                .map(o -> o.getEmail()).collect(Collectors.toList());
        MailUtil.send(email, "love - life Pro 爱生活 财务日报", content.toString(), true);
    }

}
