package com.zou.after.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zou.after.config.DynamicJobConfig;
import com.zou.after.entity.DynamicTask;
import com.zou.after.service.IDynamicTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class StartTask implements ApplicationRunner {

    @Autowired
    private IDynamicTaskService iDynamicTaskService;
    @Autowired
    private DynamicJobConfig dynamicJob;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(" ____   __    ___  _  _    __    ___  \n" +
                "(_  _) /__\\  / __)( )/ )  /  )  / _ \\ \n" +
                "  )(  /(__)\\ \\__ \\ )  (    )(  ( (_) )\n" +
                " (__)(__)(__)(___/(_)\\_)  (__)()\\___/ ");
        dynamicJob.refresh(iDynamicTaskService.list(new QueryWrapper<DynamicTask>().lambda().eq(DynamicTask::getStatus,1)));
    }
}
