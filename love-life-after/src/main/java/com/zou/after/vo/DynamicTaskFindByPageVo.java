package com.zou.after.vo;

import com.zou.after.entity.DynamicTask;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DynamicTaskFindByPageVo extends DynamicTask {
}
