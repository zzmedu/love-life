package com.zou.after.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HomeAweekDiscountVo {
    private String date;
    private String incomeMoney;
    private String payMoney;
}
