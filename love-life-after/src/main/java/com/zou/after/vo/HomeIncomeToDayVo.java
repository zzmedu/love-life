package com.zou.after.vo;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class HomeIncomeToDayVo {
    private List<String> moneys;
    private List<String> names;

    public void setMoneys(String moneys) {
        this.moneys = Arrays.asList(moneys.split(","));
    }

    public void setNames(String names) {
        this.names = Arrays.asList(names.split(","));
    }
}
