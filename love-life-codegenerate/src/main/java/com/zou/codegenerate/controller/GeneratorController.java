package com.zou.codegenerate.controller;

import com.zou.codegenerate.common.Action;
import com.zou.codegenerate.common.GeneratorParam;
import com.zou.codegenerate.common.Result;
import com.zou.codegenerate.entity.DatasourceConfig;
import com.zou.codegenerate.gen.GeneratorConfig;
import com.zou.codegenerate.service.DatasourceConfigService;
import com.zou.codegenerate.service.GeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanghc
 */
@RestController
@RequestMapping("generate")
public class GeneratorController {

    @Autowired
    private DatasourceConfigService datasourceConfigService;

    @Autowired
    private GeneratorService generatorService;

    /**
     * 生成代码
     *
     * @param generatorParam 生成参数
     * @return 返回代码内容
     */
    @RequestMapping("/code")
    public Result code(@RequestBody GeneratorParam generatorParam) {
        int datasourceConfigId = generatorParam.getDatasourceConfigId();
        DatasourceConfig datasourceConfig = datasourceConfigService.getById(datasourceConfigId);
        GeneratorConfig generatorConfig = GeneratorConfig.build(datasourceConfig);
        return Action.ok(generatorService.generate(generatorParam, generatorConfig));
    }

}
