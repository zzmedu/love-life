package com.zou.codegenerate.gen.mysql;


import com.zou.codegenerate.gen.GeneratorConfig;
import com.zou.codegenerate.gen.SQLService;
import com.zou.codegenerate.gen.TableSelector;

public class MySqlService implements SQLService {

	@Override
	public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
		return new MySqlTableSelector(new MySqlColumnSelector(generatorConfig), generatorConfig);
	}

}
