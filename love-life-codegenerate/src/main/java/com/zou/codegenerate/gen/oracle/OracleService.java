package com.zou.codegenerate.gen.oracle;


import com.zou.codegenerate.gen.GeneratorConfig;
import com.zou.codegenerate.gen.SQLService;
import com.zou.codegenerate.gen.TableSelector;

public class OracleService implements SQLService {

	@Override
	public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
		return new OracleTableSelector(new OracleColumnSelector(generatorConfig), generatorConfig);
	}

}
