package com.zou.codegenerate.gen.postgresql;


import com.zou.codegenerate.gen.GeneratorConfig;
import com.zou.codegenerate.gen.SQLService;
import com.zou.codegenerate.gen.TableSelector;

/**
 * @author tanghc
 */
public class PostgreSqlService implements SQLService {
    @Override
    public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
        return new PostgreSqlTableSelector(new PostgreSqlColumnSelector(generatorConfig), generatorConfig);
    }

}
