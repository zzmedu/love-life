package com.zou.common.enums;

/**
 * @BelongsProject: love-life
 * @BelongPackage: com.zou.common.enums
 * @Author: zouzhimin
 * @Date: 2020/11/25 10:38
 * @Description: TODO
 **/
public enum ResultEnum {

    /**
     * 成功
     */
    REQUESTSUBMIT(302, "请求太过频繁，请稍后在请求");

    private Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
