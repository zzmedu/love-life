package com.zou.common.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Objects;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 下午 4:29 2019/4/3 0003
 * </P>
 **/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionProcessor {

    /**
     * 用于处理通用异常(主要用于参数校验)
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.OK)
    public String bindException(MethodArgumentNotValidException e) throws JsonProcessingException {
        BindingResult bindingResult = e.getBindingResult();
        return new ObjectMapper().writeValueAsString(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public String nullPointException(NullPointerException e) throws JsonProcessingException {
        log.error("<===================================================================================>");
        log.error("【错误信息】" + e.getLocalizedMessage());
        log.error("<===================================================================================>");
        return new ObjectMapper().writeValueAsString(e.getLocalizedMessage());
    }

    @ExceptionHandler(RestExceptionHandler.class)
    public String restExceptionHandler(RestExceptionHandler e) throws JsonProcessingException {
        log.error("<===================================================================================>");
        log.error("【错误信息】" + e.getLocalizedMessage());
        log.error("<===================================================================================>");
        return new ObjectMapper().writeValueAsString(e.getLocalizedMessage());
    }
}
