/**
 @Name：公共组件封装
 @Author：zouzhimin
 @Site：http://www.layui.com/admin/
 */
layui.define(['table', 'laydate', 'form', 'util', 'upload'], function (exports) {
    var $ = layui.$,
        layer = layui.layer,
        laytpl = layui.laytpl,
        setter = layui.setter,
        view = layui.view,
        table = layui.table,
        admin = layui.admin,
        laydate = layui.laydate,
        util = layui.util,
        form = layui.form,
        upload = layui.upload;

    var common = {
        //置顶
        fixbar: function () {
            util.fixbar({
                css: {
                    right: 50,
                    bottom: 100
                },
                bgcolor: '#393D49'
            });
        },
        //时间组件渲染
        dateTemplate: function (elem, range) {
            laydate.render({
                elem: '#' + elem,
                range: range,
                theme: '#393D49',
                calendar: true
            });
        },
        //表格组件渲染
        tableTemplate: function (options) {
            table.render({
                method: "post",
                contentType: 'application/json',
                headers: {
                    authorization: layui.data(layui.setter.tableName).authorization
                },
                elem: options.elem,
                url: layui.setter.webSite + options.url,
                cols: options.cols,
                where: options.model,
                page: true,
                limit: 30,
                toolbar: true,
                even: true,
                limits: [30, 90, 200, 500, 1000],
                totalRow: true
            });
        },
        //表格排序
        tableSort: function (filterId, tableId) {
            table.on('sort(' + filterId + ')', function (obj) {
                table.reload(tableId, {
                    initSort: obj,
                    where: {
                        field: obj.field,
                        type: obj.type
                    }
                });
            });
        },
        //表格上面的搜索栏
        searchTemplate: function (tableId) {
            form.on('submit(btnSearch)', function (data) {
                //执行重载
                layui.table.reload(tableId, {
                    where: data.field
                });
            });
        },
        //excel文件上传
        excelFileUpload: function (options) {
            upload.render({
                elem: options.elem,
                url: layui.setter.webSite + options.url,
                accept: 'file',
                exts: 'xls|xlsx',
                headers: {
                    "authorization": layui.data(layui.setter.tableName).authorization
                },
                before: function (obj) {
                    layer.load(2);
                },
                done: function (res, index, upload) {
                    if (res.code == 0) {
                        layer.msg(res.msg);
                    } else {
                        layer.msg(res.msg);
                    }
                    layer.closeAll('loading');
                },
                error: function (index, upload) {
                    layer.msg('数据正在系统后台录入，请稍等一会在进行查询');
                    layer.closeAll('loading');
                }
            });
        },
        //文件下载,请求头加入token
        fileDownload: function (options) {
            var xhr = new XMLHttpRequest();
            var formData = new FormData();
            //post请求，参考：https://blog.csdn.net/elie_yang/article/details/79580533
            xhr.open('get', options.url);
            xhr.setRequestHeader(layui.setter.request.tokenName, layui.data(layui.setter.tableName).authorization);
            xhr.responseType = 'blob';
            xhr.onload = function (e) {
                if (this.status == 200) {
                    var blob = this.response;
                    if (window.navigator.msSaveOrOpenBlob) {
                        navigator.msSaveBlob(blob, options.filename);
                    } else {
                        var a = document.createElement('a');
                        var url = window.webkitURL.createObjectURL(blob);
                        a.href = url;
                        a.download = options.filename;
                        document.body.appendChild(a);
                        a.click();
                        window.URL.revokeObjectURL(url);
                    }

                }
            };
            xhr.send(formData);
        },
        /**
         * 批量删除
         */
        batchDelete: function (options) {
            var checkStatus = layui.table.checkStatus(options.tableId).data;
            var ids = [];
            for (var i = 0; i < checkStatus.length; i++) {
                ids.push(checkStatus[i].id)
            }
            ids = ids.join(',');

            if (this.isEmpty(ids)) {
                layer.msg("请选择需要删除的数据");
                return;
            }

            admin.req({
                type: "get",
                url: layui.setter.webSite + options.batchDelUrl + ids,
                success: function (result) {
                    if (result.code === 0) {
                        layui.table.reload(options.tableId);
                        layer.msg(result.msg);
                    } else {
                        layer.msg(result.msg);
                    }
                }
            });
        },
        //监听表格；授权、删除、修改等操作
        updateOrDeleteTemplate: function (options) {
            table.on('tool(' + options.tableId + ')', function (obj) {
                var data = obj.data;
                // 授权
                if (obj.event === 'oauth') {
                    admin.popupRight({
                        title: '授权',
                        area: ['25%', '100%'],
                        id: 'updatePage',
                        success: function (layero, index) {
                            view(this.id).render(options.oauthPage, data);
                        }
                    });
                    //删除
                } else if (obj.event === 'del') {
                    layer.confirm('确定删除？', function (index) {
                        admin.req({
                            type: "get",
                            url: layui.setter.webSite + options.delUrl + data.id,
                            success: function (result) {
                                if (result.code === 0) {
                                    obj.del();
                                    layer.msg(result.msg);
                                } else {
                                    layer.msg(result.msg);
                                }
                            }
                        });
                        layer.close(index);
                    });
                    //修改
                } else if (obj.event === 'edit') {
                    admin.popupRight({
                        title: '修改',
                        area: ['25%', '100%'],
                        id: 'updatePage',
                        success: function (layero, index) {
                            view(this.id).render(options.updatePage, data).done(function () {
                                form.on('submit(btnAdd)', function (data) {
                                    admin.req({
                                        type: "post",
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        url: layui.setter.webSite + options.updateUrl,
                                        data: JSON.stringify(data.field),
                                        success: function (result) {
                                            if (result.code === 0) {
                                                layui.table.reload(options.tableId);
                                                layer.msg(result.msg);
                                            } else {
                                                layer.msg(result.msg);
                                            }
                                        }
                                    });
                                    layer.close(index);
                                });
                            });
                        }
                    });
                }
            });
        },
        //新增操作
        addTemplate: function (options) {
            var active = {
                //新增
                add: function () {
                    admin.popupRight({
                        title: "新增",
                        area: ['25%', '100%'],
                        id: 'addPage',
                        success: function (layero, index) {
                            view(this.id).render(options.addPage).done(function () {
                                form.on('submit(btnAdd)', function (data) {
                                    admin.req({
                                        type: "post",
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        url: layui.setter.webSite + options.addUrl,
                                        data: JSON.stringify(data.field),
                                        success: function (result) {
                                            if (result.code === 0) {
                                                layer.msg(result.msg);
                                                layui.table.reload(options.tableId);
                                            } else {
                                                layer.msg(result.msg);
                                            }
                                        }
                                    });
                                    layer.close(index);
                                });
                            });
                        }
                    });
                }
            };
            //监听按钮类型，根据不同按钮，执行不同操作
            $('.layui-btn.layuiadmin-btn-useradmin').on('click', function () {
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });
        },
        //修改状态
        updateStatus: function (options) {
            form.on('switch('+options.filter+')', function (data) {
                const status = this.checked ? '1' : '2';
                admin.req({
                    type: "post",
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: layui.setter.webSite + options.updateStatusUrl + "/" + data.value + "/" + status,
                    data: JSON.stringify(data.field),
                    success: function (result) {
                        if (result.code === 0) {
                            data.elem.checked = status;
                            layer.msg(result.msg);
                            //layui.table.reload(options.tableId);
                        } else {
                            layer.msg(result.msg);
                        }
                    }
                });
                return false;
            });
        },
        //普通表格中的状态
        status: function (status, id, filter,context) {
            var checkbox = status == 1 ? 'checked' : '';
            return "<input type='checkbox' value=" + id + " lay-skin='switch' lay-filter='"+filter+"' lay-text='" + context + "' " + checkbox + "/> ";
        },
        //表格图片
        imgTable: function (value) {
            return "<img src='" + value +
                "' width='30' height='30' onclick='javascript:layui.admin.popup({type: 1,title: [\"图片展示\"],shadeClose: true,content: \"<img src=" +
                value + " height=100% width=100% />\" });' />";
        },
        //判断是否为空
        isEmpty: function (value) {
            if (typeof value == "undefined" || value == null || value == "" || value.length == 0) {
                return true;
            } else {
                return false;
            }
        }
    };

    //退出
    admin.events.logout = function () {
        //执行退出接口
        admin.req({
            url: layui.setter.webSite + '/logout',
            type: 'get',
            data: {},
            //这里要说明一下：done 是只有 response 的 code 正常才会执行。而 succese 则是只要 http 为 200 就会执行
            done: function (res) {
                //清空本地记录的 token，并跳转到登入页
                admin.exit();
            }
        });
    };

    //对外暴露的接口
    exports('common', common);
});
