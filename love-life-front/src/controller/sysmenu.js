/**
 @Name：菜单管理
 @Author：zouzhimin
 */
layui.define(['form', 'tree'], function(exports) {

	var admin = layui.admin,
		view = layui.view,
		form = layui.form,
		$ = layui.$,
		tree = layui.tree;

	//初始化菜单树
	treeLoading();

	function treeLoading() {
		admin.req({
			url: layui.setter.webSite+"/sysMenu/findByPage",
			success: function(result) {
				// 树级渲染
				tree.render({
					elem: '#menuList',
					id: "sysMenuList",
					data: result.data,
					accordion: true,
					edit: ['add', 'update', 'del'],
					operate: function(obj) {

						var type = obj.type; //得到操作类型：add、edit、del
						var data = obj.data; //得到当前节点的数据

						if (type === 'add') { //增加节点
							var obj = new Object();
							obj.parentId = data.id;
							admin.popup({
								title: "子级菜单新增",
								id: 'addPage',
								area: ['500px', '550px'],
								success: function(layero, index) {
									view(this.id).render('sysmenu/SysMenuForm', obj).done(function() {
										form.on('submit(btnAdd)', function(data) {
											admin.req({ //提交 Ajax 成功后，关闭当前弹层并重载表格
												type: "post",
												url: layui.setter.webSite+"/sysMenu/add",
												contentType: "application/json; charset=utf-8",
												dataType: 'json',
												cache: false,
												async: true,
												data: JSON.stringify(data.field),
												success: function(result) {
													if (result.code === 0) {
														layer.msg(result.msg);
														treeLoading();
													} else {
														layer.msg(result.msg);
													}
												}
											});
											layer.close(index);
										});
									});
								}
							});
						} else if (type === 'update') {
							admin.popup({
								title: '修改',
								area: ['500px', '550px'],
								id: 'updatePage',
								success: function(layero, index) {
									view(this.id).render('sysmenu/SysMenuForm', data).done(function() {
										form.on('submit(btnAdd)', function(data) {
											admin.req({
												type: "post",
												url: layui.setter.webSite+"/sysMenu/update",
												contentType: "application/json; charset=utf-8",
												dataType: 'json',
												cache: false,
												async: true,
												data: JSON.stringify(data.field),
												success: function(result) {
													if (result.code === 0) {
														treeLoading();
														layer.msg(result.msg);
													} else {
														layer.msg(result.msg);
													}
												}
											});
											layer.close(index);
										});
									});
								}
							});
						} else if (type === 'del') {
							admin.req({
								type: "get",
								url: layui.setter.webSite+"/sysMenu/delete/" + obj.data.id,
								success: function(result) {
									if (result.code === 0) {
										treeLoading();
										layer.msg(result.msg);
									} else {
										layer.msg(result.msg);
									}
								}
							});
						};
					}
				});
			}
		});
	}

	// 新增父级菜单
	var active = {
		add: function() {
			admin.popupRight({
				title: '新增父级菜单',
				area: ['25%', '100%'],
				id: 'addPage',
				success: function(layero, index) {
					view(this.id).render('sysmenu/SysMenuForm').done(function() {
						//监听提交
						form.on('submit(btnAdd)', function(data) {
							var field = data.field; //获取提交的字段

							admin.req({
								type: "post",
								url: layui.setter.webSite+"/sysMenu/add",
								contentType: "application/json; charset=utf-8",
								dataType: 'json',
								cache: false,
								async: true,
								data: JSON.stringify(data.field),
								success: function(result) {
									if (result.code === 0) {
										treeLoading();
										layer.msg(result.msg);
									} else {
										layer.msg(result.msg);
									}
								}
							});
							layer.close(index);
						});
					});
				}
			});
		}
	};

	/**
	 * 签收率统计
	 * 销售统计
	 * 出货统计
	 */
	$('.layui-btn.layuiadmin-btn-useradmin').on('click', function() {
		var type = $(this).data('type');
		active[type] ? active[type].call(this) : '';
	});

	exports('sysmenu', {})
});
