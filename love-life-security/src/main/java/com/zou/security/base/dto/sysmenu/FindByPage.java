package com.zou.security.base.dto.sysmenu;

import com.zou.security.base.pojo.SysMenu;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 10:41 2020/8/12
 * </P>
 **/
@Setter
@Getter
public class FindByPage extends SysMenu {

    private Long id;

}
