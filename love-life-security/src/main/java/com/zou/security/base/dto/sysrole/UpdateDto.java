package com.zou.security.base.dto.sysrole;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 18:28 2020/8/11
 * </P>
 **/
@Setter
@Getter
public class UpdateDto extends AddDto {

    @NotNull(message = "编号不能为空")
    private Long id;
}
