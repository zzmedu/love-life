package com.zou.security.base.dto.sysuser;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 9:35 2020/8/11
 * </P>
 **/
@Setter
@Getter
public class AddDto {

    @NotBlank(message = "账号不能为空")
    private String username;

    private String password;

    @NotBlank(message = "昵称不能为空")
    private String nikName;

    @NotBlank(message = "语言不能为空")
    private String lang;

    private String email;

    @NotNull(message = "请选择角色")
    private Long roleId;

}
