package com.zou.security.base.enums;

/**
 * @BelongsProject: zlog-spring-boot-start
 * @BelongPackage: org.zzm.zlog.enums
 * @Author: zouzhimin
 * @Date: 2020/11/18 14:51
 * @Description: TODO
 **/
public enum HttpEnum {
    /**
     * 成功
     */
    SUCCESS(0, "success"),

    /**
     * 失败
     */
    fail(-1,"fail"),

    /**
     * 登录失败
     */
    LOGINFAIL(401,"错误：账号或密码不正确"),

    LOGOUTSUS(302,"退出成功"),

    /**
     * 未登录，直接返回改字符串
     */
    ACCESSFAILED(1001,"请先登录，在进行访问"),

    /**
     * 系统异常处理
     */
    SYSTEMERROR(500,"系统异常，请稍后重试..."),

    NOPERMISSION(403,"无权限，请联系管理员"),

    /**
     * 已存在
     */
    ISEXIS(301,"已存在");

    private Integer code;

    private String msg;

    HttpEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
