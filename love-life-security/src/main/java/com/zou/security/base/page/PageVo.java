package com.zou.security.base.page;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class PageVo<T> {
    private List<T> result;
    private Long total;
}
