package com.zou.security.base.util;


import com.zou.security.base.vo.sysmenu.FindByPageVo;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 11:02 2020/8/12
 * </P>
 **/
public class TreeUtils {

    /**
     * 生成菜单
     *
     * @param sysMenus
     *@return
     */
    public static List getMenu(List<FindByPageVo> sysMenus) {
        //实例化一个根节点数组
        List<FindByPageVo> root = new ArrayList<>();
        for (FindByPageVo sysMenu : sysMenus) {
            //数据库查出pid为0或空，就直接加入到根节点
            if (sysMenu.getParentId() == 0) {
                root.add(sysMenu);
            }
        }
        for (FindByPageVo sysMenu : root) {
            sysMenu.setChildren(getChildren(sysMenus, sysMenu));
        }
        return root;
    }

    /**
     * 生成树枝
     *
     * @param sysMenus  生成菜单传过来的剩余数据
     * @param root 根节点对象
     * @return
     */
    public static List<FindByPageVo> getChildren(List<FindByPageVo> sysMenus, FindByPageVo root) {
        List<FindByPageVo> children = new ArrayList<>();
        for (FindByPageVo sysMenu : sysMenus) {
            if (root.getId().equals(sysMenu.getParentId())) {
                children.add(sysMenu);
            }
        }
        for (FindByPageVo child : children) {
            child.setChildren(getChildren(sysMenus, child));
        }
        return children;
    }
}
