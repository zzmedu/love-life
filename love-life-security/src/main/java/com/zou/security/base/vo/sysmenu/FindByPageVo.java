package com.zou.security.base.vo.sysmenu;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
public class FindByPageVo {

    private Long id;

    private String title;

    private String titleZh;

    private String path;

    private String icon;

    private String label;

    private Long level;

    private Long parentId;

    private Boolean isMenu;

    private Boolean checked;

    private List<FindByPageVo> children = new ArrayList<>();

    public void add(FindByPageVo findByPage) {
        children.add(findByPage);
    }

    public void setTitle(String title) {
        if(!ObjectUtils.isEmpty(isMenu) && !StringUtils.isEmpty(label)) {
            this.title = title + '【' + this.label + '】';
        }else{
            this.title = title;
        }
    }
}

