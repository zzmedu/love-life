package com.zou.security.base.vo.sysuser;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FindByPageVo {

    private Long id;

    private String username;

    private String email;

    private String nikName;

    private Boolean accountNonExpired;

    private Boolean enabled;

    private String lang;

    private String nameZh;
}
