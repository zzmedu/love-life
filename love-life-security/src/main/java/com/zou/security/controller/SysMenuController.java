package com.zou.security.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zou.security.base.dto.sysmenu.AddDto;
import com.zou.security.base.dto.sysmenu.UpdateDto;
import com.zou.security.base.http.Response;
import com.zou.security.base.http.Request;
import com.zou.security.base.pojo.SysMenu;
import com.zou.security.base.util.TreeUtils;
import com.zou.security.service.ISysMenuService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zzm.zlog.inter.ZLog;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@RestController
@RequestMapping("/sysMenu")
public class SysMenuController {

    @Autowired
    private ISysMenuService iSysMenuService;

    @GetMapping("/findByPage")
    @ZLog(value = "菜单分页")
    public Response findByPage(String id){
        return Request.success(TreeUtils.getMenu(iSysMenuService.findByPage(id)));
    }

    @PostMapping("/add")
    @ZLog(value = "菜单新增")
    public Response add(@Valid @RequestBody AddDto add) {
        SysMenu sysMenu = new SysMenu();
        BeanUtils.copyProperties(add, sysMenu);
        if (iSysMenuService.save(sysMenu)) {
            return Request.success();
        }
        return Request.error();
    }

    @PostMapping("/update")
    @ZLog(value = "菜单修改")
    public Response update(@Valid @RequestBody UpdateDto update) {
        SysMenu sysMenu = new SysMenu();
        BeanUtils.copyProperties(update, sysMenu);
        if (iSysMenuService.updateById(sysMenu)) {
            return Request.success();
        }
        return Request.error();
    }

    @GetMapping("/delete/{id}")
    @ZLog(value = "菜单删除")
    public Response delete(@PathVariable String id) {
        if (iSysMenuService.remove(new QueryWrapper<SysMenu>().lambda().eq(SysMenu::getId, id))) {
            //删除子级菜单
            iSysMenuService.remove(new QueryWrapper<SysMenu>().lambda().eq(SysMenu::getParentId,id));
            return Request.success();
        }
        return Request.error();
    }

    @GetMapping("/leftSideMenu/{roleName}")
    @ZLog(value = "页面左侧菜单")
    public Response leftSideMenu(@PathVariable String roleName) {
        return Request.success(TreeUtils.getMenu(iSysMenuService.leftSideMenu(roleName)));
    }

}

