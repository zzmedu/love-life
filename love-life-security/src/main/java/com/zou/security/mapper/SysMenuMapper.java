package com.zou.security.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zou.security.base.vo.sysmenu.FindByPageVo;
import com.zou.security.base.pojo.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<FindByPageVo> findByPage(@Param("id") Long id);

    List<SysMenu> findMenuByUserName(@Param("roleName") String roleName);

    List<FindByPageVo> leftSideMenu(@Param("roleName") String roleName);
}
