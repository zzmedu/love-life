package com.zou.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zou.security.base.dto.sysuser.SysUserDetailsDto;
import com.zou.security.base.pojo.SysUser;
import com.zou.security.base.vo.sysuser.FindByPageVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
    IPage<FindByPageVo> findByPage(Page<SysUser> page);
    SysUserDetailsDto findUserByUsername(@Param("userName") String userName);
}
