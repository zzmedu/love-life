package com.zou.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.security.base.pojo.SysRoleMenu;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
